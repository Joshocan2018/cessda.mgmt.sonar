# README #

# SonarQube #

This repository contains all the scripts and informations needed to deploy and manage
CESSDA SonarQube platform

## Getting Started

### Prerequisites

- Gcloud SDK and kubectl should be installed on your machine

- Gcloud credentials should be set

- Database (Postgresql) should be available (both GCP managed database and self-managed database works)

## Contributing

Please read [CESSDA Guideline for developpers](https://bitbucket.org/cessda/cessda.guidelines.cit/wiki/Developers) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

## Authors

* **Julien Le Hericy** - *Initial work*

You can find the list of all contributors [here](CONTRIBUTORS.md)

## License

This project is licensed under the Apache 2 License - see the [LICENSE](LICENSE) file for details

## Acknowledgments
