#!/bin/bash

#############################
#          CESSDA           #
#  Generic Cluster Setup    #
#############################


### Load Env Variable and set gcloud Region/Zone/Project
source ./gcp.config > /dev/null 2>&1

### some debug statements
echo "Client: $CLIENT"
echo "Product: $PRODUCT"
echo "Module: $MODULE"
echo "Environment: $ENVIRONMENT"

# Kubctl credentials setup
gcloud container clusters get-credentials $CLIENT-$PRODUCT-$ENVIRONMENT-cc --zone=$ZONE > /dev/null 2>&1
#gcloud container clusters get-credentials cessda-jenkins-mgmt-live-cluster --zone=$ZONE > /dev/null 2>&1

# Disk Creation
if gcloud compute disks list 2> /dev/null | grep -E "$cessda-sonarqube-pd" > /dev/null 2>&1;
  then
    echo "$MODULE disk already exists"
  else
    gcloud compute disks create cessda-sonarqube-pd --size 10GB
    echo "$MODULE disk created"
fi;

#Persistent Volume Creation
if kubectl get pv -n $CLIENT-$PRODUCT-$ENVIRONMENT 2> /dev/null | grep -E "cessda-sonarqube-pv" > /dev/null 2>&1;
  then
    echo "$MODULE Persistent Volume already exist"
  else
    kubectl create -f ../k8s/cessda-sonar-pv.yaml
    echo "$MODULE Persistent Volume created"
fi;

#Persistent Volume Claim
if kubectl get pvc -n $CLIENT-$PRODUCT-$ENVIRONMENT 2> /dev/null | grep -E "cessda-sonarqube-pvc" > /dev/null 2>&1;
  then
    echo "$MODULE Persistent Volume Claim already exist"
  else
    kubectl create -f ../k8s/cessda-sonar-pvc.yaml
    echo "$MODULE Persistent Volume Claim created"
fi;

#Secret creation
if kubectl get secret cessda-sonar-secret -n $CLIENT-$PRODUCT-$ENVIRONMENT
  then
    echo "Secret already exists"
  else
    kubectl create -f ../k8s/cessda-sonar-secret.yaml
	echo "Secret created"
fi;


# Deployment
if kubectl get deployment $CLIENT-$PRODUCT-sonar-$ENVIRONMENT -n $CLIENT-$PRODUCT-$ENVIRONMENT
  then
    echo "Deployment already exists, it will be destroyed to perform the new deployment"

    kubectl delete deployment $CLIENT-$PRODUCT-sonar-$ENVIRONMENT -n $CLIENT-$PRODUCT-$ENVIRONMENT
    echo "Deployment deleted"
fi;

# create the deployment
kubectl create -f ../k8s/cessda-sonar-deployment.yaml
echo "Deployment created"

# Service
if kubectl get service $CLIENT-$PRODUCT-sonar-$ENVIRONMENT -n $CLIENT-$PRODUCT-$ENVIRONMENT > /dev/null 2>&1;
  then
    echo "Service already exists"
  else
    kubectl create -f ../k8s/cessda-sonar-service.yaml
    echo "Service created"
fi;

# create a Db for the sonarqube 
#chmod +x ./sonar-creation.sh
#./sonar-creation.sh


